from datetime import datetime

from django.test import TestCase, Client
from django.urls import reverse

from users.models import User
from .models import Term


def create_user():
    return User.objects.create_user(
        username="John",
        first_name="John",
        last_name="Doe",
        street="Street",
        post_code="12345",
        password="12345"
    )


class SignTermViewTests(TestCase):
    def setUp(self):
        self.user = create_user()
        self.client = Client()

    def test_signing_terms_first_time(self):
        self.client.login(username="John", password="12345")
        response = self.client.post(reverse('terms-of-service:sign-list'))
        self.assertEqual(response.status_code, 200)

    def test_signing_terms_two_times(self):
        self.client.login(username="John", password="12345")
        self.client.post(reverse('terms-of-service:sign-list'))
        first_date = self.user.agreement.sign_date
        self.client.post(reverse('terms-of-service:sign-list'))
        self.assertEqual(first_date, self.user.agreement.sign_date)


class DisplayAgreementViewTest(TestCase):
    def setUp(self):
        self.user = create_user()
        self.client = Client()

    def test_display_not_existing_agreement(self):
        self.client.login(username="John", password="12345")
        response = self.client.get(reverse('terms-of-service:display-detail', args={1}))
        self.assertEqual(response.status_code, 404)

    def test_display_existing_agreement(self):
        self.client.login(username="John", password="12345")
        agreement = Term.objects.create(
            user=self.user
        )
        response = self.client.get(reverse('terms-of-service:display-detail', args={agreement.id}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.first_name)
        self.assertContains(response, self.user.last_name)
        self.assertContains(response, self.user.street)
        self.assertContains(response, self.user.post_code)
