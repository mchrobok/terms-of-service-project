from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin

from .models import Term
from .serializers import TermsSerializer


class SignTermsViewSet(CreateModelMixin, GenericViewSet):
    queryset = Term.objects.all()
    serializer_class = TermsSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = TermsSerializer(data=request.data)
        if serializer.is_valid():
            agreement = serializer.save(user=request.user)
            return HttpResponseRedirect(redirect_to=reverse('terms-of-service:display-detail', args=(agreement.id,)))
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class DisplayTermsViewSet(RetrieveModelMixin, GenericViewSet):
    queryset = Term.objects.all()
    serializer_class = TermsSerializer
    permission_classes = [IsAuthenticated]
    template_name = 'terms/agreement.html'
    renderer_classes = [TemplateHTMLRenderer]

    def retrieve(self, request, pk):
        agreement = get_object_or_404(self.queryset, pk=pk)
        return Response({'agreement': agreement})
