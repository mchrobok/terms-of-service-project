from django.db import models
from django.utils import timezone

from users.models import User


class Term(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="agreement")
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    street = models.CharField(max_length=250)
    post_code = models.CharField(max_length=10)
    sign_date = models.DateTimeField(editable=False)

    def __str__(self):
        return f"User: {self.first_name} {self.last_name} has signed Terms of Service on {self.sign_date}"

    def save(self, *args, **kwargs):
        """
        Adds to Term instance such information as first_name, last_name, street and post_code from user's profile
        :param args:
        :param kwargs:
        :return:
        """
        if self.user:
            self.first_name = self.user.first_name
            self.last_name = self.user.last_name
            self.street = self.user.street
            self.post_code = self.user.post_code
            self.sign_date = timezone.now()

        super().save(*args, **kwargs)
