from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import SignTermsViewSet, DisplayTermsViewSet

router = DefaultRouter()
router.register('', SignTermsViewSet, basename='sign')
router.register('', DisplayTermsViewSet, basename='display')

app_name = 'terms-of-service'
urlpatterns = [
    path('', include(router.urls)),
]
