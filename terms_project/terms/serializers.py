from rest_framework import serializers

from .models import Term


class TermsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Term
        fields = []

    def create(self, validated_data):
        user = validated_data.get("user")
        if getattr(user, "agreement", None):
            return user.agreement

        result = super().create(validated_data)
        return result

