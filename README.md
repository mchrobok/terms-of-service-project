#Installing

To install project, please use commands:

`docker-compose -f docker-compose.yaml build`  
`docker-compose -f docker-compose.yaml up -d`


# Using
Signing the agreement is available just for logged in users, in the database are prepared two users:

login: `admin` password: `admin`  
login: `John` password: `password`
  
You can login using `localhost:8000/api-auth/login/?next=/terms-of-service/`


When you use `localhost:8000/terms-of-service/` using `POST` method (or hit the `POST` button on under that endpoint) as a logged-in user, the agreement would be signed for you with the current date, and you will be redirected to view with agreement template.

On the `localhost:8000/terms-of-service/agreement/` you can see the template with user data and date when the agreement has been signed. 


With the `docker-compose exec backend bash -c "./manage.py test"` command, you can run the tests.

Using admin panel `localhost:8000/admin` logged-is as `admin` user you can browse through users and signed agreements (collection `Terms`)

